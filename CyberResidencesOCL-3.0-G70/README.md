CyberResidencesOCL
==================

Group
-----
### FILL THIS SECTION AS SHOWN BELOW AND LINES STARTING WITH ###
@author Michael HORAK <horak.90@gmail.com>
@author Valerio POSTIGLIONE <valerio.postiglione@gmail.com>
@group  G70
@spreadsheet https://docs.google.com/spreadsheets/d/11gRFOnJyI9Z03RVETkJp7h96ybLM1naWdKDSHy4QI1k/edit#gid=730179076 

Current state of the specification
----------------------------------
The specification is fully implemented. All of constraints are written in the '.use' file.
All the constraints seem to be correctly working according to the tests performed.
The limitations in our specification are given by the fact that each state file targets at respecting or breaking only the concerned constraint.
For this reason, often the mutation code for one constraint would break another constraint. But we assume this is normal and acceptable, since
the goal of each state file is just to prove the issued constraint. Furthermore, the base soil file provides a set of data that respects all the 
constraints.
Overall we are satisfied of the results and we found the exercise very helpful in order to understand OCL better. 
In fact, it makes a huge different to work on the computer on the things we saw in class and modelled on the paper.


###Explain what is implemented, what is not
### what is good in your specification, what are the limitations



Testing strategy
----------------
The testing phase has been divided into two parts. 
At the beginning, we have built a soil base set of data which would respect all the constraints.
In order to do that, we used the excel-filling approach and we generated each time the soil file. Thanks to that we managed to find out several errors
in our constraints implementation and we were able to fix them. At the same time, this phase helped us choosing the right data and familiarizing with
the OCL syntax.
The second part of our testing concerned the writing of each couple of state files, which targeted each constraint specifically. This was made by simply
getting inspired by the generated base soil file, and just adding some mutation code with new data. The state files were tested one by one after each
implementation.
As for the team work management, we decided to divide the constraint, and each one of us worked on half of the constraints. Nevertheless, we were able to
check each other's constraint and to help each other when some issues occurred.

### FILL THIS SECTION
### Explain briefly the strategy chosen to organise tests, implement tests, etc.


Current state of the tests
--------------------------
The tests are all implemented and correctly working.
Both the base soil file and the several state files respond correctly to the aim of the tests (i.e. the base soil file respects all the constraints, 
the OK state files invoke a respectful execution of the script, while the KO state files generate failures for the corresponding constraint).

### FILL THIS SECTION 
### What is implemented, what is not


